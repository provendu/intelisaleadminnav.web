import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MaterialModule } from './material.module';
import { EventService } from './services/event.service';
import { SidebarService } from './services/sidebar.service';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule
  ],
  declarations: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent
  ],
  entryComponents: [],
  exports: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent

  ],
  providers: [
    EventService,
    SidebarService
  ]
})
export class IntelisaleadminnavModule {}
