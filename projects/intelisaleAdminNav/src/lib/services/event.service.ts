import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class EventService {

  public eventSubject = new Subject<string>();
  public event$ = this.eventSubject.asObservable();

  broadcast(e: any) {
    this.eventSubject.next(e);
  }
}
