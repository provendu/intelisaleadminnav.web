import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class SidebarService {
  public preparedUi: any;

  constructor(
    private router: Router
  ) { }

  prepareSideMenu(clientModules, permissions) {
    this.preparedUi = [];

    clientModules[0].modules.forEach((module) => {
      if(module.name === "catalog-menu" && permissions.find((permission) => permission.name === "catalog-menu")) {
        let obj = {
          placeholder: 'dashboardCatalog',
          collapsed: false,
          subModule: []
        }

        clientModules[0].modules.forEach((module) => {
          if(module.name === "catalog-products" && permissions.find((permission) => permission.name === "catalog-products")) {
            let subObj = {
              placeholder: 'dashboardCatalogProducts',
              image: 'products-sidebar-img',
              link: '/catalog/products/regular'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "competitive-products" && permissions.find((permission) => permission.name === "catalog-products")) {
            let subObj = {
              placeholder: 'dashboardCatalogCompetitiveProducts',
              image: 'competitive-products-sidebar-img',
              link: '/catalog/products/competitive'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "planogram-products" && permissions.find((permission) => permission.name === "catalog-products")) {
            let subObj = {
              placeholder: 'dashboardCatalogPlanogramProducts',
              image: 'planogram-products-sidebar-img',
              link: '/catalog/products/planogram'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "categories" && permissions.find((permission) => permission.name === "categories")) {
            let subObj = {
              placeholder: 'dashboardCatalogCategories',
              image: 'categories-sidebar-img',
              link: '/catalog/categories'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "brand" && permissions.find((permission) => permission.name === "brand")) {
            let subObj = {
              placeholder: 'dashboardCatalogBrands',
              image: 'brands-sidebar-img',
              link: '/catalog/brands'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "manufacturers" && permissions.find((permission) => permission.name === "manufacturers")) {
            let subObj = {
              placeholder: 'dashboardCatalogManufacturers',
              image: 'manufacturers-sidebar-img',
              link: '/catalog/manufacturers'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "linked-product" && permissions.find((permission) => permission.name === "linked-product")) {
            let subObj = {
              placeholder: 'dashboardLinkedProducts',
              image: 'linked-products-sidebar-img',
              link: '/products'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "item-attributes" && permissions.find((permission) => permission.name === "item-attributes")) {
            let subObj = {
              placeholder: 'dashboardCatalogItemAttributes',
              image: 'item-attributes-sidebar-img',
              link: '/catalog/item-attributes'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "customer-planogram" && permissions.find((permission) => permission.name === "customer-planogram")) {
            let subObj = {
              placeholder: 'dashboardCatalogCustomerPlanogram',
              image: 'customer-planogram-sidebar-img',
              link: '/matrix/customer-planogram'
            }

            obj.subModule.push(subObj);
          }

          if (module.name === "mustsell" && permissions.find((permission) => permission.name === "mustsell")) {
            let subObj = {
              placeholder: 'dashboardMustsell',
              image: 'products-sidebar-img',
              link: '/catalog/mustsell'
            }

            obj.subModule.push(subObj);
          }
          // novi modul Invertar/Tools
          if(module.name === "tools" && permissions.find((permission) => permission.name === "tools")) {
            let subObj = {
              placeholder: 'dashboardCatalogTools',
              image: 'tools-sidebar-img',
              link: '/catalog/products/tools'
            }

            obj.subModule.push(subObj);
          }

        })
        this.preparedUi.push(obj);
      }

      if(module.name === "pim-menu" && permissions.find((permission) => permission.name === "pim-menu")) {
        let obj = {
          placeholder: 'dashboardPim',
          collapsed: false,
          subModule: []
        }

        clientModules[0].modules.forEach((module) => {
          if(module.name === "pim-products" && permissions.find((permission) => permission.name === "pim-products")) {
            let subObj = {
              placeholder: 'dashboardPimProducts',
              image: 'products-sidebar-img',
              link: '/pim/pim-products'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "pim-categories" && permissions.find((permission) => permission.name === "pim-categories")) {
            let subObj = {
              placeholder: 'dashboardPimCategories',
              collapsed: false,
              image: 'categories-sidebar-img',
              subModulesOfSubObj: []
            }

            clientModules[0].modules.forEach((module) => {
              if(module.name === "pim-categories" && permissions.find((permission) => permission.name === "pim-categories")) {
                let subModuleOfSubObj = {
                  placeholder: 'dashboardPimCategoriyList',
                  image: 'categories-sidebar-img',
                  link: '/pim/pim-categories'
                }

                subObj.subModulesOfSubObj.push(subModuleOfSubObj);
              }

              if(module.name === "pim-categories-tree" && permissions.find((permission) => permission.name === "pim-categories-tree")) {
                let subModuleOfSubObj = {
                  placeholder: 'dashboardPimCategoryHierarchy',
                  image: 'categories-tree-sidebar-img',
                  link: '/pim/category-tree/list'
                }

                subObj.subModulesOfSubObj.push(subModuleOfSubObj);
              }

            })
            obj.subModule.push(subObj);
          }

          if(module.name === "pim-attributes" && permissions.find((permission) => permission.name === "pim-attributes")) {
            let subObj = {
              placeholder: 'dashboardPimAttributes',
              collapsed: false,
              image: 'item-attributes-sidebar-img',
              subModulesOfSubObj: []
            }

            clientModules[0].modules.forEach((module) => {
              if(module.name === "pim-attributes" && permissions.find((permission) => permission.name === "pim-attributes")) {
                let subModuleOfSubObj = {
                  placeholder: 'dashboardPimSpecificationAttributes',
                  image: 'item-attributes-sidebar-img',
                  link: '/pim/pim-attributes'
                }

                subObj.subModulesOfSubObj.push(subModuleOfSubObj);
              }

              if(module.name === "hidden-light-streams" && permissions.find((permission) => permission.name === "pim-attributes")) {
                let subModuleOfSubObj = {
                  placeholder: 'dashboardPimVariantAttributes',
                  link: '/pim/pim-variant-attributes'
                }

                subObj.subModulesOfSubObj.push(subModuleOfSubObj);
              }



              if(module.name === "pim-sets-attributes") {
                let subModuleOfSubObj = {
                  placeholder: 'dashboardPimAttributeSets',
                  link: '/pim/sets-attributes'
                }

                subObj.subModulesOfSubObj.push(subModuleOfSubObj);
              }

            })

            obj.subModule.push(subObj);
          }

          if(module.name === "pim-manufacturers" && permissions.find((permission) => permission.name === "pim-manufacturers")) {
            let subObj = {
              placeholder: 'dashboardPimManufacturers',
              image: 'manufacturers-sidebar-img',
              link: '/pim/pim-manufacturers'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "pim-brand" && permissions.find((permission) => permission.name === "pim-brands")) {
            let subObj = {
              placeholder: 'dashboardPimBrands',
              image: 'brands-sidebar-img',
              link: '/pim/pim-brands'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "hidden-light-streams" && permissions.find((permission) => permission.name === "pim-upload-console")) {
            let subObj = {
              placeholder: 'dashboardUploadConsole',
              image: 'upload-console-sidebar-img',
              link: '/pim/upload-console'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "hidden-light-streams" && permissions.find((permission) => permission.name === "pim-export")) {
            let subObj = {
              placeholder: 'dashboardExport',
              image: 'export-sidebar-img',
              link: '/pim/export'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "hidden-light-streams" && permissions.find((permission) => permission.name === "pim-settings")) {
            let subObj = {
              placeholder: 'dashboardSettings',
              image: 'settings-sidebar-img',
              link: '/pim/settings'
            }

            obj.subModule.push(subObj);
          }
        })
        this.preparedUi.push(obj);

      }

      if(module.name === "admin-customers-menu" && permissions.find((permission) => permission.name === "admin-customers-menu")) {
        let obj = {
          placeholder: 'dashboardAdminCustomers',
          collapsed: false,
          subModule: []
        }

        clientModules[0].modules.forEach((module) => {
          if(module.name === "admin-customers" && permissions.find((permission) => permission.name === "admin-customers")) {
            let subObj = {
              placeholder: 'dashboardCustomers',
              image: 'admin-customers-sidebar-img',
              link: '/admin-customers/customers'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "customer-categories" && permissions.find((permission) => permission.name === "customer-categories")) {
            let subObj = {
              placeholder: 'dashboardCustomerCategories',
              image: 'customer-categories-sidebar-img',
              link: '/admin-customers/customer-categories'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "customers-and-employees-relations" && permissions.find((permission) => permission.name === "customers-and-employees-relations")) {
            let subObj = {
              placeholder: 'dashboardCustomersAndEmployeesRelations',
              image: 'customers-and-employees-relations-sidebar-img',
              link: '/admin-customers/customers-and-employees-relations'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "cities" && permissions.find((permission) => permission.name === "cities")) {
            let subObj = {
              placeholder: 'dashboardCities',
              image: 'cities-sidebar-img',
              link: '/admin-customers/cities'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "customer-attributes" && permissions.find((permission) => permission.name === "customer-attributes")) {
            let subObj = {
              placeholder: 'dashboardCustomerAttributes',
              image: 'customer-attributes-sidebar-img',
              link: '/admin-customers/customer-attributes'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "contact-attributes" && permissions.find((permission) => permission.name === "contact-attributes")) {
            let subObj = {
              placeholder: 'dashboardContactAttributes',
              image: 'customer-attributes-sidebar-img',
              link: '/admin-customers/contact-attributes'
            }
            obj.subModule.push(subObj);
          }
        })
        this.preparedUi.push(obj);
      }

      if(module.name === "custom-list-and-matrix-menu" && permissions.find((permission) => permission.name === "custom-list-and-matrix-menu")) {
        let obj = {
          placeholder: 'dashboardCustomListAndMatrix',
          collapsed: false,
          subModule: []
        }

        clientModules[0].modules.forEach((module) => {
          if(module.name === "custom-list" && permissions.find((permission) => permission.name === "custom-list")) {
            let subObj = {
              placeholder: 'dashboardCustomList',
              image: 'custom-list-sidebar-img',
              link: '/custom-list'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "matrix-items" && permissions.find((permission) => permission.name === "matrix-items")) {
            let subObj = {
              placeholder: 'dashboardItemMatrix',
              image: 'item-matrix-sidebar-img',
              link: '/matrix/items'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "matrix-customlists" && permissions.find((permission) => permission.name === "matrix-customlists")) {
            let subObj = {
              placeholder: 'dashboardCustomListMatrix',
              image: 'custom-list-matrix-sidebar-img',
              link: '/matrix/customlists'
            }

            obj.subModule.push(subObj);
          }
        })
        this.preparedUi.push(obj);
      }

      if(module.name === "activities-menu" && permissions.find((permission) => permission.name === "activities-menu")) {
        let obj = {
          placeholder: 'dashboardActivities',
          collapsed: false,
          subModule: []
        }

        clientModules[0].modules.forEach((module) => {
          if(module.name === "inventory" && permissions.find((permission) => permission.name === "inventory")) {
            let subObj = {
              placeholder: 'dashboardCreatedInventories',
              image: 'created-inventories-sidebar-img',
              link: '/inventory/created-inventory'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "inventory-done" && permissions.find((permission) => permission.name === "inventory")) {
            let subObj = {
              placeholder: 'dashboardDoneInventories',
              image: 'done-inventories-sidebar-img',
              link: '/inventory/done-inventory'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "surveys" && permissions.find((permission) => permission.name === "surveys")) {
            let subObj = {
              placeholder: 'dashboardCreatedSurveys',
              image: 'created-survey-sidebar-img',
              link: '/surveys/created'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "surveys-done" && permissions.find((permission) => permission.name === "surveys")) {
            let subObj = {
              placeholder: 'dashboardDoneSurveys',
              image: 'done-survey-sidebar-img',
              link: '/surveys/done'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "admin/all-activities" && permissions.find((permission) => permission.name === "activities/view-all")) {
            let subObj = {
              placeholder: 'dashboardAllActivities',
              image: 'all-activities-sidebar-img',
              link: '/all-activities'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "user-activity" && permissions.find((permission) => permission.name === "user-activity")) {
            let subObj = {
              placeholder: 'userActivities',
              image: 'user-activity-sidebar-img',
              link: '/user-activities'
            }

            obj.subModule.push(subObj);
          }
        })
        this.preparedUi.push(obj);
      }

      if(module.name === "system-administration-menu" && permissions.find((permission) => permission.name === "system-administration-menu")) {
        let obj = {
          placeholder: 'systemAdministrationDashboard',
          collapsed: false,
          subModule: []
        }

        clientModules[0].modules.forEach((module) => {
          if(module.name === "roles-and-permissions" && permissions.find((permission) => permission.name === "user-roles")) {
            let subObj = {
              placeholder: 'dashboardRolesAndPermissions',
              image: 'roles-and-permissions-sidebar-img',
              link: '/roles-and-permissions'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "user-management" && permissions.find((permission) => permission.name === "users")) {
            let subObj = {
              placeholder: 'dashboardUserManagement',
              image: 'user-management-sidebar-img',
              link: '/user-management'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "working-days" && permissions.find((permission) => permission.name === "working-days")) {
            let subObj = {
              placeholder: 'dashboardWorkingDays',
              image: 'working-days-planning-sidebar-img',
              link: '/admin/working-days'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "upload-console" && permissions.find((permission) => permission.name === "upload-console")) {
            let subObj = {
              placeholder: 'dashboardUploadConsole',
              image: 'upload-console-sidebar-img',
              link: '/admin/upload-console'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "tags" && permissions.find((permission) => permission.name === "tags")) {
            let subObj = {
              placeholder: 'dashboardTags',
              image: 'tags-sidebar-img',
              link: '/tags'
            }

            obj.subModule.push(subObj);

          }
          if(module.name === "start-day-types" && permissions.find((permission) => permission.name === "start-day-types")) {
            let subObj = {
              placeholder: 'dashboardStartDayTypes',
              image: 'customer-attributes-sidebar-img',
              link: 'start-day-types'
            }

            obj.subModule.push(subObj);
          }

          if(module.name === "activity-types-administration" && permissions.find((permission) => permission.name === "activity-types-administration")) {
            let subObj = {
              placeholder: 'dashboardActivityTypes',
              image: 'customer-attributes-sidebar-img',
              link: '/activity-types'
            }

            obj.subModule.push(subObj);
          }

        })
        this.preparedUi.push(obj);
      }
    });

    return this.preparedUi;
  }

  navigateTo(link: string) {
    this.router.navigate([link]);
  }

}
