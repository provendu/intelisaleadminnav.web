import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { EventService } from '../../services/event.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() environment: any;
  @Input() currentUser: any
  @Output() logout = new EventEmitter<any>();

  @HostBinding('class.search-toggled') search = false;

  public sidebarActive = false;

  constructor(
    private eventService: EventService
  ) {}

  ngOnInit() {
    this.subscribeAll();
  }

  subscribeAll() {
    this.eventService.event$.subscribe(event => {
      if (event === 'open-sidebar') {
        this.sidebarActive = true;
      }
      if (event === 'close-sidebar') {
        this.sidebarActive = false;
      }
    });
  }

  toggleSidebar() {

    if (this.sidebarActive) {
      this.eventService.broadcast('close-sidebar');
    } else {
      this.eventService.broadcast('open-sidebar');
    }

  }

  openSearch() {
    this.search = true;
  }

  closeSearch() {
    this.search = false;
  }

  logoutClicked() {
    this.logout.emit();
  }

  public getImageUrl(employeeCode: string) {
    return employeeCode ? this.environment.image_folder + 'slikeKorisnika/' + employeeCode + '.jpg' : 'assets/img/avatar.png';
  }

  imageError(e: any, img = 'assets/img/item.png') {
    e.target.src = img;
  }

}
