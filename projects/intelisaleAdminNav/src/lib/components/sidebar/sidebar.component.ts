import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { EventService } from '../../services/event.service';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  animations: [
    trigger('heroState', [
      state('inactive', style({
        transform: 'translateX(-100%)'
      })),
      state('active', style({
        transform: 'translateX(0)'
      })),
      transition('inactive <=> active', animate('200ms ease-out'))
    ])
  ]
})
export class SidebarComponent implements OnInit {

  @HostBinding('class.toggled') isActive = false;
  @Output() toggled = new EventEmitter<boolean>();
  @Output() navigateLink = new EventEmitter<string>();
  @Input() sideMenuList: any;

  public adminCustomerCollapsed = false;
  public activitiesCollapsed = false;
  public catalogCollapsed = false;
  public customListAndMatrixCollapsed = false;
  public systemAdministrationCollapsed = false;
  public pimCollapsed = false;
  public pimCategorySubMenuCollapsed = false;

  private productsToggled = 'inactive';
  public path: string;

  constructor(
    private eventService: EventService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.path = this.router.url;

    this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.path = this.router.url;
      }
    });

    this.eventService.event$.subscribe(event => {
      if (event === 'open-sidebar') {
        this.isActive = true;
        this.toggled.emit(this.isActive);
      }
      if (event === 'close-sidebar') {
        this.isActive = false;
        this.toggled.emit(this.isActive);
      }
    });
  }

  toggleState() {
    this.productsToggled = (this.productsToggled === 'active' ? 'inactive' : 'active');
  }

  navigateTo(path: string) {
    this.navigateLink.emit(path);
  }
}
