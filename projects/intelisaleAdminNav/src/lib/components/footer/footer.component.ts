import { Component, Input } from '@angular/core';

@Component({
  selector: 'footer',
  templateUrl: 'footer.component.html'
})

export class FooterComponent {

  @Input() environment: any;

  currentYear = new Date().getFullYear();

  constructor() {}

}
